package com.empresa.demo.service;

import com.empresa.demo.model.Student;
import com.empresa.demo.repository.StundetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StundetsRepository studentRepository;

    @Autowired
    public StudentService(StundetsRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Student save(Student student) {
        return studentRepository.save(student);
    }

    public Student update(Student student) {
        return studentRepository.save(student);
    }

    public Student get(Long id) {
        return studentRepository.findById(id).orElse(null);
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public List<Student> getByName(String name) {
        return studentRepository.findAllByFirstName(name);
    }

    public void delete(Student student) {
        studentRepository.delete(student);
    }

    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }

    public Student getByNumberId(Integer numberId) {
        return studentRepository.obtenerPorNumeroId(numberId).orElse(null);
    }

    public Student getByFirstName(String firstName) {
        return studentRepository.findByFirstName(firstName).orElse(null);
    }
}
