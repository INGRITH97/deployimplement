package com.empresa.demo.repository;

import com.empresa.demo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StundetsRepository extends JpaRepository<Student, Long> {

    Optional<Student> findByFirstName(String firstName);

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "st_id, " +
                    "st_first_name, " +
                    "st_second_name, " +
                    "st_first_surname, " +
                    "st_second_surname, " +
                    "st_number, " +
                    "st_activo " +
                    "FROM students " +
                    "WHERE st_number =: dni")
    Optional<Student> obtenerPorNumeroId(@Param(value = "dni") Integer numberId);

    List<Student> findAllByFirstName(String firstName);
}
