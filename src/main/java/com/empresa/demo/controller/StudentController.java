package com.empresa.demo.controller;

import com.empresa.demo.model.Student;
import com.empresa.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/student")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentController {

    final
    StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping(path = "/save")
    public Student saveStudent(@RequestBody Student student){
       return studentService.save(student);
    }

    @PutMapping(path = "/update")
    public Student updateStudent(@RequestBody Student student){
        return studentService.update(student);
    }

    @GetMapping(path = "/get/name")
    public List<Student> obtenerStudentsByName(@RequestParam(name = "name") String name){
        return studentService.getByName(name);
    }

    @GetMapping(path = "/get")
    public List<Student> obtenerStudents(){
        return studentService.getAll();
    }

    @DeleteMapping(path = "/delete")
    public String deleteStudent(@RequestParam(name = "id") Long id){
        studentService.deleteById(id);
        return "eliminado";
    }

}
