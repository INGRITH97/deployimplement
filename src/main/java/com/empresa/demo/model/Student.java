package com.empresa.demo.model;



import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "st_id")
    private Long id;

    @Column(name = "st_first_name")
    private String firstName;

    @Column(name = "st_second_name")
    private String secondName;

    @Column(name = "st_first_surname")
    private String firstSurname;

    @Column(name = "st_second_surname")
    private String secondSurname;

    @Column(name = "st_number")
    private Integer numberId;

    @Column(name = "st_activo")
    private Boolean activo;

}
